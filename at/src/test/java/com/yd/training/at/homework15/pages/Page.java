package com.yd.training.at.homework15.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public abstract class Page {

    protected final WebDriver driver;

    public Page(WebDriver driver) {
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    protected void waitForElementVisibility(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

    protected void waitForUrlContains(String fraction) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.urlContains(fraction));
    }

    protected void waitForElementClickable(By locator) {
        new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(locator));
    }

    protected void waitForUrlNotContains(String fraction) {
        new WebDriverWait(driver, 20).until(ExpectedConditions.not(ExpectedConditions.urlContains(fraction)));
    }
}