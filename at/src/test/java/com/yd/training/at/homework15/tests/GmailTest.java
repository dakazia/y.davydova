package com.yd.training.at.homework15.tests;

import com.yd.training.at.homework15.models.Letter;
import com.yd.training.at.homework15.models.User;
import com.yd.training.at.homework15.pages.*;
import com.yd.training.at.homework15.utils.Utils;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;


public class GmailTest extends ConfigurationTest {

    private static final By EMAIL_ADDRESS_ELEMENT = By.xpath("//div[@role='region']//div[@tabindex='1']//span[@email]");
    private static final By EMAIL_SUBJECT_ELEMENT = By.xpath("//h2[@class='a3E']");
    private static final By EMAIL_MESSAGE_ELEMENT = By.xpath("//div[@role='textbox']");
    private static final String LOG_OFF_URL_ELEMENT = "signin";

    @Test(priority = 1, groups = "logOnLogOff" )
    public void loginTest() {
        User testUser = User.modelUser();
        InboxPage inboxPage = new LoginPage(driver).loginToAccount(testUser);
        Assert.assertTrue(inboxPage.getTextFromCurrentUrl().contains(LoginPage.INBOX_PAGE_URL_ELEMENT));
    }

    @Test(priority = 2)
    public void saveEmailInDraftTest() {
        Letter testLetter = Letter.modelLetter();
        DraftPage draftPage = new InboxPage(driver).createDraft(testLetter);
        Assert.assertTrue(draftPage.getAllSubjectDraft(Utils.INDEX_OF_EMAIL));
    }

    @Test(priority = 3)
    public void emailAddressOfDraftTest() {
        Assert.assertTrue(driver.findElement(EMAIL_ADDRESS_ELEMENT)
                .getText().contains(Letter.EMAIL_ADDRESS));
    }

    @Test(priority = 3)
    public void emailSubjectOfDraftTest() {
        Assert.assertTrue(driver.findElement(EMAIL_SUBJECT_ELEMENT)
                .getText().contains(Utils.INDEX_OF_EMAIL));
    }

    @Test(priority = 3)
    public void emailTextMessageOfDraftTest() {
        Assert.assertTrue(driver.findElement(EMAIL_MESSAGE_ELEMENT)
                .getText().contains(Letter.TEXT_MESSAGE));
    }

    @Test(priority = 4)
    public void emailOutFromDraftPageTest() {
        DraftPage draftPage = new DraftPage(driver);
        Assert.assertFalse(draftPage.sendEmail(Utils.INDEX_OF_EMAIL));
    }

    @Test(priority = 5)
    public void mailIsInSentPageTest() {
        SentPage sendMail = new DraftPage(driver).openSentPage();
        Assert.assertTrue(sendMail.getAllSubjectSentEmail(Utils.INDEX_OF_EMAIL));
    }

    @Test(priority = 6, groups = "logOnLogOff" )
    public void logOutTest() {
        NavigationMenu logOut = new NavigationMenu(driver).logOffFromAccount(LOG_OFF_URL_ELEMENT);
        Assert.assertTrue(logOut.getTextFromCurrentUrlLogOff().contains(LOG_OFF_URL_ELEMENT));
    }

}

