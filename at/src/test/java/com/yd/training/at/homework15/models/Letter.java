package com.yd.training.at.homework15.models;

import com.yd.training.at.homework15.service.MessageCreator;
import com.yd.training.at.homework15.utils.Utils;

import java.util.Objects;

public class Letter {

    public static final String EMAIL_ADDRESS = "dyx.afi@gmail.com";
    private static final int ADD_SYMBOLS_MESSAGE = 4;
    public static final String TEXT_MESSAGE = MessageCreator.generateRandomMessage(ADD_SYMBOLS_MESSAGE);

    public static Letter modelLetter (){
        return new Letter(EMAIL_ADDRESS, Utils.INDEX_OF_EMAIL, TEXT_MESSAGE);
    }

    private String emailAddress;
    private String subject;
    private String textMessage;


    private Letter(String emailAddress, String subject, String textMessage) {
        this.emailAddress = emailAddress;
        this.subject = subject;
        this.textMessage = textMessage;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }
    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getTextMessage() {
        return textMessage;
    }

    public void setTextMessage(String textMessage) {
        this.textMessage = textMessage;
    }


    @Override
    public String toString() {
        return "Letter{" +
                "emailAddress='" + emailAddress + '\'' +
                ", subject='" + subject + '\'' +
                ", testMessage='" + textMessage  +
        '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Letter)) return false;
        Letter letter = (Letter) o;
        return Objects.equals(getEmailAddress(), letter.getEmailAddress()) &&
                Objects.equals(getSubject(), letter.getSubject())&&
                Objects.equals(getTextMessage(), letter.getTextMessage());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEmailAddress(), getSubject(), getTextMessage());
    }
}
