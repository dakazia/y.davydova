package com.yd.training.at.homework15.pages;

import com.yd.training.at.homework15.models.Letter;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class InboxPage extends NavigationMenu {

    private static final By EMAIL_ADDRESS_ELEMENT = By.xpath("//textarea[@role='combobox']");
    private static final By EMAIL_SUBJECT_ELEMENT = By.xpath("//input[@name='subjectbox']");
    private static final By EMAIL_MESSAGE_ELEMENT = By.xpath("//div[@role='textbox']");
    private static final By COMPOSE_EMAIL_ELEMENT = By.xpath("//div[@class='nH bkL']//div[@role='button']");
    private static final By SAVE_DRAFT_ELEMENT = By.xpath("//img[@class='Ha']");

    public InboxPage(WebDriver driver) {
        super(driver);
    }

    public String getTextFromCurrentUrl() {
        return driver.getCurrentUrl();
    }

    private void setEmailAddressElement(Letter letter) {
        driver.findElement(EMAIL_ADDRESS_ELEMENT).sendKeys(letter.getEmailAddress());
    }

    private void setSubjectEmailElement(Letter letter) {
        driver.findElement(EMAIL_SUBJECT_ELEMENT).sendKeys(letter.getSubject());
    }

    private void setEmailMessageElement(Letter letter) {
        driver.findElement(EMAIL_MESSAGE_ELEMENT).sendKeys(letter.getTextMessage());
    }

    public DraftPage createDraft(Letter letter) {
        waitForElementClickable(COMPOSE_EMAIL_ELEMENT);
        driver.findElement(COMPOSE_EMAIL_ELEMENT).click();
        setEmailAddressElement(letter);
        setSubjectEmailElement(letter);
        setEmailMessageElement(letter);
        driver.findElement(SAVE_DRAFT_ELEMENT).click();
        return new DraftPage(driver);
    }
}

