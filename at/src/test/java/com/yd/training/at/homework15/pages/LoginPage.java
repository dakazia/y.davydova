package com.yd.training.at.homework15.pages;

import com.yd.training.at.homework15.models.User;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Page {

    private static final By USER_ELEMENT = By.id("identifierId");
    private static final By IDENTIFIER_NEXT_BUTTON = By.id("identifierNext");
    private static final By PASSWORD_ELEMENT = By.name("password");
    private static final By SIGN_IN_BUTTON = By.id("passwordNext");
    public static final String INBOX_PAGE_URL_ELEMENT = "#inbox";

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    private void setUserName(User user) {
        driver.findElement(USER_ELEMENT).sendKeys(user.getUsername());
        driver.findElement(IDENTIFIER_NEXT_BUTTON).click();
    }

    private void setPassword(User user) {
        driver.findElement(PASSWORD_ELEMENT).sendKeys(user.getPassword());
        driver.findElement(SIGN_IN_BUTTON).click();
    }

    public InboxPage loginToAccount(User user) {
        setUserName(user);
        setPassword(user);
        waitForUrlContains(INBOX_PAGE_URL_ELEMENT);
        return new InboxPage(driver);
    }


}
