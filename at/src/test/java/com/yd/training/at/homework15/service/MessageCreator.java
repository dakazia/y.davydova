package com.yd.training.at.homework15.service;

import java.util.Random;

public class MessageCreator {

    private static final String ALFANUMERICAL = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private static Random random = new Random();

    private static String getRandomString(int stringLength) {
        StringBuilder stringBuilder = new StringBuilder(stringLength);
        for (int i = 0; i < stringLength; i++) {
            stringBuilder.append(ALFANUMERICAL.charAt(random.nextInt(ALFANUMERICAL.length())));
        }
        return stringBuilder.toString();
    }

    public static String generateRandomMessage(int addSymbols) {
        return "Nice day #".concat(getRandomString(addSymbols));
    }
}
